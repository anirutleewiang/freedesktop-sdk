variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  GET_SOURCES_ATTEMPTS: 3
  BST_CACHE_SERVER_ADDRESS: 'freedesktop-sdk-cache.codethink.co.uk'
  BST_SHA: 'c270d09a5bb473a3a7f2f7243440a306b792bc89' #  1.2.4
  BST_EXTERNAL_SHA: '0.9.0-0-g63a19e8068bd777bd9cd59b1a9442f9749ea5a85'
  RUNTIME_VERSION: '18.08'

  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_AMD64: "${DOCKER_REGISTRY}/amd64:087a6159febf5e3375efdaee300adb3b09f6f69e"
  DOCKER_AARCH64: "${DOCKER_REGISTRY}/aarch64:087a6159febf5e3375efdaee300adb3b09f6f69e"

  # Generic variable for invoking buildstream
  BST: bst --colors

stages:
  - update
  - flatpak
  - vm
  - prepare-publish
  - publish
  - finish-publish

before_script:
  - export PATH=~/.local/bin:${PATH}
  - git clone https://gitlab.com/BuildStream/buildstream.git
  - git -C buildstream checkout "${BST_SHA}"
  - pip3 install --user buildstream/
  - git clone https://gitlab.com/BuildStream/bst-external.git
  - git -C bst-external checkout "${BST_EXTERNAL_SHA}"
  - pip3 install --user ./bst-external

  # Configure Buildstream
  - mkdir -p ~/.config
  - |
    cat > ~/.config/buildstream.conf << EOF
    # Get a lot of output in case of errors
    logging:
      error-lines: 80
    EOF

  # Create ~/.ssh for storing keys
  - mkdir -p ~/.ssh

  # Private key stored as a protected variable that allows pushing to
  # cache.sdk.freedesktop.org
  - |
    if [ -n "$freedesktop_ostree_cache_private_key" ]; then
        echo "$freedesktop_ostree_cache_private_key" > ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
        ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
    fi

  # Create CAS directory for SSL keys
  - mkdir -p /etc/ssl/CAS

  # Private SSL keys/certs for pushing to the CAS server
  - |
    if [ -n "$GITLAB_CAS_PUSH_CERT" ] && [ -n "$GITLAB_CAS_PUSH_KEY" ]; then
        echo "$GITLAB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
        echo "$GITLAB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key

        echo "projects:" >> ~/.config/buildstream.conf
        echo "  freedesktop-sdk-bootstrap:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        if [ -f /cache-certificate/server.crt ]; then
            echo "    - url: https://local-cas-server:1102" >> ~/.config/buildstream.conf
            echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
            echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
            echo "      server-cert: /cache-certificate/server.crt"  >> ~/.config/buildstream.conf
            echo "      push: true" >> ~/.config/buildstream.conf
        fi
        echo "    - url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
        echo "  freedesktop-sdk:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        if [ -f /cache-certificate/server.crt ]; then
            echo "    - url: https://local-cas-server:1102" >> ~/.config/buildstream.conf
            echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
            echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
            echo "      server-cert: /cache-certificate/server.crt"  >> ~/.config/buildstream.conf
            echo "      push: true" >> ~/.config/buildstream.conf
        fi
        echo "    - url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
    fi

  # flat-manager tokens to upload the releases
  - |
    if [ -n "$CI_COMMIT_TAG" ] && [ -n "$FLATHUB_REPO_TOKEN" ]; then
        export REPO_TOKEN=$FLATHUB_REPO_TOKEN
        export RELEASES_SERVER_ADDRESS=https://hub.flathub.org/
    elif [ -n "$RELEASES_REPO_TOKEN" ]; then
        export REPO_TOKEN=$RELEASES_REPO_TOKEN
        export RELEASES_SERVER_ADDRESS=https://cache.sdk.freedesktop.org/
    fi


# Store all the downloaded git and ostree repos in the distributed cache.
# This saves us fetching them on every build
.gitlab_cache_template_pull: &gitlab_cache_pull
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"
    policy: pull

.gitlab_cache_template_pull_push: &gitlab_cache_pull_push
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"



check_update_elements:
  image: $DOCKER_AARCH64
  stage: update
  tags:
    - check_update
  script:
    - ${BST} track --deps all flatpak-release.bst
    - git diff
  only:
    - schedules

check_update_elements:
  image: $DOCKER_AMD64
  stage: update
  tags:
    - x86_64
  script:
    - ${BST} --on-error continue -o target_arch "x86_64" -o bootstrap_build_arch "x86_64" fetch --deps all flatpak-release.bst || true
    - ${BST} --on-error continue -o target_arch "i686" -o bootstrap_build_arch "x86_64" fetch --deps all flatpak-release.bst || true
    - ${BST} --on-error continue -o target_arch "aarch64" -o bootstrap_build_arch "aarch64" fetch --deps all flatpak-release.bst || true
    - ${BST} --on-error continue -o target_arch "arm" -o bootstrap_build_arch "aarch64" fetch --deps all flatpak-release.bst || true
  <<: *gitlab_cache_pull_push
  only:
    - schedules

.flatpak_template: &flatpak_definition
  stage: flatpak
  script:
    - make ARCH=${ARCH} build
    - make ARCH=${ARCH} check-dev-files
    - make ARCH=${ARCH} check-rpath

    - export FLATPAK_USER_DIR="${PWD}/tmp-flatpak"
    - make ARCH=${ARCH} export
    - make ARCH=${ARCH} test-apps

    - |
      REFERENCE=$(git merge-base ${RUNTIME_VERSION} ${CI_COMMIT_SHA}) && \
      ./utils/check-abi --old=${REFERENCE} --new=${CI_COMMIT_SHA} abi/desktop-abi-image.bst


  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
    - tags
  <<: *gitlab_cache_pull

app_x86_64:
  image: $DOCKER_AMD64
  <<: *flatpak_definition
  tags:
    - x86_64
    - cache_x86_64
  variables:
    ARCH: x86_64

app_i686:
  image: $DOCKER_AMD64
  <<: *flatpak_definition
  tags:
    - x86_64
    - cache_i686
  variables:
    ARCH: i686

app_aarch64:
  image: $DOCKER_AARCH64
  <<: *flatpak_definition
  tags:
    - aarch64
  variables:
    ARCH: aarch64

app_arm:
  image: $DOCKER_AARCH64
  <<: *flatpak_definition
  tags:
    - armhf
  variables:
    ARCH: arm

.vm_imageless_template: &vm_imageless
  stage: vm
  script:
    - make build-vm
    - utils/test-minimal-system --dialog "${DIALOG}" command 'make run-vm'
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
    - tags
  <<: *gitlab_cache_pull

minimal_systemd_vm_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_imageless
  variables:
    ARCH: x86_64
    VM_ARTIFACT: vm/minimal-systemd-vm.bst
    DIALOG: root-login

minimal_systemd_vm_i686:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_i686
  <<: *vm_imageless
  variables:
    ARCH: i686
    VM_ARTIFACT: vm/minimal-systemd-vm.bst
    DIALOG: root-login

minimal_systemd_vm_aarch64:
  image: $DOCKER_AARCH64
  tags:
    - aarch64
  <<: *vm_imageless
  variables:
    ARCH: aarch64
    VM_ARTIFACT: vm/minimal-systemd-vm.bst
    DIALOG: root-login
  when: manual # skip aarch64 VM tests by default for now; the runner is too slow

minimal_systemd_vm_arm:
  image: $DOCKER_AARCH64
  tags:
    - armhf
  <<: *vm_imageless
  variables:
    ARCH: arm
    VM_ARTIFACT: vm/minimal-systemd-vm.bst
    DIALOG: root-login

.vm_image_template: &vm_image
  stage: vm
  script:
    - ${BST} -o target_arch "${ARCH}" build vm/"${TYPE}"-vm-image-"${ARCH}".bst
    - ${BST} -o target_arch "${ARCH}" checkout vm/"${TYPE}"-vm-image-"${ARCH}".bst ./vm
    - utils/test-minimal-system --dialog "${DIALOG}" image vm/sda.img
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - schedules
  <<: *gitlab_cache_pull

minimal_vm_image_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal
    DIALOG: minimal

minimal_systemd_vm_image_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal-systemd
    DIALOG: root-login

.desktop_vm_image_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_image
  allow_failure: true
  variables:
    ARCH: x86_64
    TYPE: desktop
    DIALOG: root-login

prepare_publish:
  image: $DOCKER_AMD64
  stage: prepare-publish
  tags:
    - x86_64
  script:
    - git clone https://github.com/flatpak/flat-manager.git
    - flat-manager/flat-manager-client create $RELEASES_SERVER_ADDRESS stable > publish_build.txt
  artifacts:
    paths:
      - publish_build.txt
  only:
    - master
    - '18.08'
    - tags
  except:
    - schedules

finish_publish:
  image: $DOCKER_AMD64
  stage: finish-publish
  tags:
    - x86_64
  script:
    - git clone https://github.com/flatpak/flat-manager.git
    - flat-manager/flat-manager-client commit --wait $(cat publish_build.txt)
    - flat-manager/flat-manager-client publish --wait $(cat publish_build.txt)
    - flat-manager/flat-manager-client purge $(cat publish_build.txt)
  only:
    - tags
    - master
    - '18.08'
  except:
    - schedules

finish_publish_failed:
  image: $DOCKER_AMD64
  stage: finish-publish
  tags:
    - x86_64
  script:
    - git clone https://github.com/flatpak/flat-manager.git
    - flat-manager/flat-manager-client purge $(cat publish_build.txt)
  only:
    - tags
    - master
    - '18.08'
  except:
    - schedules
  when: on_failure

.flatpak_publish_template:
  stage: publish
  retry: 2
  script:
    - make ARCH=${ARCH} build
    - make ARCH=${ARCH} export

    - |
      for ref in $(ostree --repo=repo refs --list); do
        case "${ref}" in
          */org.freedesktop.Sdk*/*/18.08) ;&
          */org.freedesktop.Platform*/*/18.08)
            echo "Keeping ${ref}"
          ;;
          */*/*/*)
            echo "Deleting ${ref}"
            ostree --repo=repo refs --delete "${ref}"
          ;;
        esac
      done
    - flatpak build-update-repo --generate-static-deltas --prune repo
    - git clone https://github.com/flatpak/flat-manager.git
    - flat-manager/flat-manager-client push $(cat publish_build.txt) repo
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - tags
    - master
    - '18.08'
  except:
    - schedules

publish_x86_64:
  extends: .flatpak_publish_template
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  variables:
    ARCH: x86_64

publish_i686:
  extends: .flatpak_publish_template
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_i686
  variables:
    ARCH: i686

publish_aarch64:
  extends: .flatpak_publish_template
  image: $DOCKER_AARCH64
  tags:
    - aarch64
  variables:
    ARCH: aarch64

publish_arm:
  extends: .flatpak_publish_template
  image: $DOCKER_AARCH64
  tags:
    - armhf
  variables:
    ARCH: arm

cve_report:
  stage: publish
  image: $DOCKER_AMD64
  cache:
    key: cve
    paths:
      - "${XDG_CACHE_HOME}/cve"
  tags:
    - x86_64
    - cache_x86_64
  script:
    - pip3 install --user lxml

    - make manifest

    - mkdir -p "${XDG_CACHE_HOME}/cve"
    - cd "${XDG_CACHE_HOME}/cve"
    - python3 "${CI_PROJECT_DIR}/utils/update-local-cve-database.py"

    - mkdir -p "${CI_PROJECT_DIR}/cve-reports"
    - python3 "${CI_PROJECT_DIR}/utils/generate-cve-report.py" "${CI_PROJECT_DIR}/sdk-manifest/usr/manifest.json" "${CI_PROJECT_DIR}/cve-reports/sdk.html"
    - python3 "${CI_PROJECT_DIR}/utils/generate-cve-report.py" "${CI_PROJECT_DIR}/platform-manifest/usr/manifest.json" "${CI_PROJECT_DIR}/cve-reports/platform.html"
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/cve-reports"
  only:
    - master
    - '18.08'

markdown_manifest:
  stage: publish
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  script:
    - make markdown-manifest
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/platform-manifest/usr/"
      - "${CI_PROJECT_DIR}/sdk-manifest/usr/"
  only:
    - master
    - '18.08'
  except:
    - schedules
